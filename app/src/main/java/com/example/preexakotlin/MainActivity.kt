package com.example.preexakotlin

//import android.R
import android.app.AlertDialog
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {
    private lateinit var btnEntrar: Button
    private lateinit var btnSalir: Button
    private lateinit var txtNombre: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        iniciarComponentes()

        btnEntrar.setOnClickListener {
            entrar()
        }

        btnSalir.setOnClickListener {
            salir()
        }
    }

    private fun iniciarComponentes() {
        btnEntrar = findViewById(R.id.entrarButton)
        btnSalir = findViewById(R.id.salirButton)
        txtNombre = findViewById(R.id.txtNombre)
    }

    private fun entrar() {
        val strNombre = txtNombre.text.toString()
        if (txtNombre.text.toString().isEmpty()) {
            Toast.makeText(applicationContext, "Nombre no válido o no ingresado", Toast.LENGTH_LONG).show()
        } else {
            val bundle = Bundle()
            bundle.putString("Nombre", strNombre)

            val intent = Intent(this@MainActivity, ReciboActivity::class.java)
            intent.putExtras(bundle)
            startActivity(intent)
        }
        txtNombre.setText("")
    }

    private fun salir() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage("¿Deseas cerrar la app?")
        builder.setPositiveButton(
            "Sí"
        ) { dialog, which -> finish() }
        builder.setNegativeButton("No", null)
        builder.show()
    }

}
