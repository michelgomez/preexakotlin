package com.example.preexakotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

class ReciboActivity : AppCompatActivity() {
    private var btnCalcular: Button? = null
    private var btnLimpiar: Button? = null
    private var btnRegresar: Button? = null
    private var txtHorasTrabajadas: EditText? = null
    private var txtNombre: EditText? = null
    private var txtNumRecibo: EditText? = null
    private var txtHorasExtras: EditText? = null
    private var lblSubtotal: TextView? = null
    private var lblImpuesto: TextView? = null
    private var lblTotal: TextView? = null
    private var rdbAuxiliar: RadioButton? = null
    private var rdbAlbanil: RadioButton? = null
    private var rdbIngObra: RadioButton? = null

    private var recibo = ReciboNomina(0, "", 0f, 0f, 0, 0f)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recibo)

        iniciarComponentes()
        val bundle = intent.extras
        txtNombre?.setText(bundle?.getString("Nombre"))

        rdbAuxiliar?.setOnClickListener { Auxiliar() }
        rdbAlbanil?.setOnClickListener { Albanil() }
        rdbIngObra?.setOnClickListener { IngObra() }
        btnRegresar?.setOnClickListener { regresar() }
        btnLimpiar?.setOnClickListener { limpiar() }
        btnCalcular?.setOnClickListener { calcular() }
    }

    private fun iniciarComponentes() {
        btnCalcular = findViewById(R.id.btnCalcular)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnRegresar = findViewById(R.id.btnRegresar)
        txtNumRecibo = findViewById(R.id.txtNumRecibo)
        txtHorasExtras = findViewById(R.id.txtHorasExtras)
        txtHorasTrabajadas = findViewById(R.id.txtHorasTrabajadas)
        lblSubtotal = findViewById(R.id.lblSubtotal)
        lblImpuesto = findViewById(R.id.lblImpuesto)
        lblTotal = findViewById(R.id.lblTotal)
        rdbAuxiliar = findViewById(R.id.rdbAuxiliar)
        rdbAlbanil = findViewById(R.id.rdbAlbanil)
        rdbIngObra = findViewById(R.id.rdbIngObra)
        txtNombre = findViewById(R.id.txtNombre)
    }

    fun Auxiliar() {
        recibo.setPuesto(1)
    }

    fun Albanil() {
        recibo.setPuesto(2)
    }

    fun IngObra() {
        recibo.setPuesto(3)
    }

    fun limpiar() {
        // Limpia los campos de entrada y los valores de lblSubtotal, lblImpuesto y lblTotal
        txtNumRecibo?.setText("")
        txtHorasTrabajadas?.setText("")
        txtHorasExtras?.setText("")
        lblSubtotal?.setText("")
        lblImpuesto?.setText("")
        lblTotal?.setText("")
        rdbAuxiliar?.isChecked = false
        rdbAlbanil?.isChecked = false
        rdbIngObra?.isChecked = false
    }

    private fun regresar() {
        val confirmar: AlertDialog.Builder = AlertDialog.Builder(this)
        confirmar.setTitle("Cálculo de nómina")
        confirmar.setMessage("¿Desea regresar?")
        confirmar.setPositiveButton("Confirmar") { dialogInterface, which ->
            finish()
        }
        confirmar.setNegativeButton("Cancelar") { dialogInterface, which -> }
        confirmar.show()
    }

    private fun calcular() {
        val numReciboStr = txtNumRecibo?.text.toString().trim()
        val horasTrabajadasStr = txtHorasTrabajadas?.text.toString().trim()
        val horasExtrasStr = txtHorasExtras?.text.toString().trim()

        if (!numReciboStr.isEmpty() && !horasTrabajadasStr.isEmpty() && !horasExtrasStr.isEmpty() && (rdbAuxiliar?.isChecked == true || rdbAlbanil?.isChecked == true || rdbIngObra?.isChecked == true)) {
            val numRecibo = numReciboStr.toInt()
            val horasTrabajadas = horasTrabajadasStr.toFloat()
            val horasExtras = horasExtrasStr.toFloat()

            recibo.setNumRecibo(numRecibo)
            recibo.setHorasTrabNormal(horasTrabajadas)
            recibo.setHorasTrabExtras(horasExtras)

            val subtotal = recibo.calcularSubtotal()
            val impuesto = recibo.calcularImpuesto()
            val total = recibo.calcularTotal()

            lblSubtotal?.text = subtotal.toString()
            lblImpuesto?.text = impuesto.toString()
            lblTotal?.text = total.toString()
        } else {
            Toast.makeText(this, "Por favor, llene todos los campos", Toast.LENGTH_SHORT).show()
        }
    }
}