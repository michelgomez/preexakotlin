package com.example.preexakotlin

import androidx.appcompat.app.AppCompatActivity


class ReciboNomina(i: Int, s: String, fl: Float, fl1: Float, i1: Int, fl2: Float) : AppCompatActivity() {
    private var numRecibo = 0
    private var nombre: String? = null
    private var horasTrabNormal = 0f
    private var horasTrabExtras = 0f
    private var puesto = 0
    private var impuestoPorc = 0f

    fun ReciboNomina(
        numRecibo: Int,
        nombre: String?,
        horasTrabNormal: Float,
        horasTrabExtras: Float,
        puesto: Int,
        impuestoPorc: Float
    ) {
        this.numRecibo = numRecibo
        this.nombre = nombre
        this.horasTrabNormal = horasTrabNormal
        this.horasTrabExtras = horasTrabExtras
        this.puesto = puesto
        this.impuestoPorc = impuestoPorc
    }

    fun getNumRecibo(): Int {
        return numRecibo
    }

    fun setNumRecibo(numRecibo: Int) {
        this.numRecibo = numRecibo
    }

    fun getNombre(): String? {
        return nombre
    }

    fun setNombre(nombre: String?) {
        this.nombre = nombre
    }

    fun getHorasTrabNormal(): Float {
        return horasTrabNormal
    }

    fun setHorasTrabNormal(horasTrabNormal: Float) {
        this.horasTrabNormal = horasTrabNormal
    }

    fun getHorasTrabExtras(): Float {
        return horasTrabExtras
    }

    fun setHorasTrabExtras(horasTrabExtras: Float) {
        this.horasTrabExtras = horasTrabExtras
    }

    fun getPuesto(): Int {
        return puesto
    }

    fun setPuesto(puesto: Int) {
        this.puesto = puesto
    }

    fun getImpuestoPorc(): Float {
        return impuestoPorc
    }

    fun setImpuestoPorc(impuestoPorc: Float) {
        this.impuestoPorc = impuestoPorc
    }

    fun calcularSubtotal(): Float {
        var pagoBase = 200f
        var incrementoPago = 0f
        var pagoPorHora = 0f

        // Calcular el incremento del pago según el puesto
        when (puesto) {
            1 -> incrementoPago = 0.2f
            2 -> incrementoPago = 0.5f
            3 -> incrementoPago = 1f
        }

        // Calcular el incremento del pago base
        pagoBase += pagoBase * incrementoPago

        // Calcular el pago por horas normales
        val pagoHorasNormales = pagoBase * horasTrabNormal

        // Calcular el pago por horas extras
        if (incrementoPago > 0) {
            pagoPorHora = pagoBase
        }
        val pagoHorasExtras = pagoPorHora * horasTrabExtras * 2

        // Calcular el subtotal
        val subtotal = pagoHorasNormales + pagoHorasExtras

        return subtotal
    }

    fun calcularImpuesto(): Float {
        val subtotal = calcularSubtotal()
        impuestoPorc = 0.16f
        return subtotal * impuestoPorc
    }

    fun calcularTotal(): Float {
        val subtotal = calcularSubtotal()
        val impuesto = calcularImpuesto()
        return subtotal - impuesto
    }
}